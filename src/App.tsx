import './App.css';
import { ComicsList } from './components/ComicList';
import { Header } from './components/Header';
import { ComicListContextProvider } from './context/comic-list/ComicListContextProvider';

export const App = () => {
  return (
    <ComicListContextProvider>
      <div className="App">
        <Header/>
        <ComicsList/>
      </div>
    </ComicListContextProvider>
  );
}