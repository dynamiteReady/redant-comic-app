import { useContext } from "react";
import { ComicListContext } from "../context/comic-list/ComicListContext";

export const Header = () => {
  const comicListContext = useContext(ComicListContext);
  return <div className="header">
    <h1>Red Ant Comics</h1>
    <span>Favourites: ({comicListContext.state.favourites.length ?? 0})</span>
  </div>
}
  