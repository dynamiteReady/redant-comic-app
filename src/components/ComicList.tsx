import { useContext } from "react";
import { ComicListContext } from "../context/comic-list/ComicListContext";
import { ComicListItem } from "../context/comic-list/ComicListContextStateTypes";
import { LoadingIndicator } from "./LoadingIndicator";

export const ComicsList = () => {
  const comicListContext = useContext(ComicListContext);

  if(comicListContext?.state?.comics?.length === 0 && comicListContext?.actions) return <LoadingIndicator/>;

  const comicListState = comicListContext.state; 
  const comicListActions = comicListContext.actions; 

  const toggleFavourite = (item: ComicListItem, isSelected: boolean) => {
    if(!isSelected) {
      comicListActions.addFavourite(item);
    } else comicListActions.removeFavourite(item);
  }

  const generateComicbookItems = () => {
    return comicListState.comics?.map(item => {
      const isSelected = comicListState.favourites.includes(item);

      return <div className={`item${isSelected ? ' selected' : ''}`} key={item.id} onClick={_e => toggleFavourite(item, isSelected)}>
        <div className="title">
          { item.title }
          {/* If an item has been favourited, indicate the fact */}
          { isSelected && <span> Favourited!</span> }
        </div>
        {item?.images?.length ? generateImages(item) : <img src="https://via.placeholder.com/560x850"/>}
      </div>
    });
  };

  const generateImages = (comicItem: ComicListItem) => {
    if(!comicItem?.images ) return;

    return comicItem?.images.map(image => <img key={image.path} src={`${image.path}.${image.extension}`}/>);
  };

  return (
    <div className="comics-list">
      { generateComicbookItems() }
    </div>
  );
}
