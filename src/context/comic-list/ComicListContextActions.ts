import { ComicListItem, ComicListContextObject } from "./ComicListContextStateTypes";

// More prudent to keep this typedef with it's reducer.
// There are already too many files imo...
type ComicListActions = (
  | { type: 'SET_COMICS'; comics: ComicListItem[]; }
  | { type: 'ADD_FAVOURITE'; comic: ComicListItem; }
  | { type: 'REMOVE_FAVOURITE'; comic: ComicListItem; }
);

// Reducer...
export const comicListReducer = (context: ComicListContextObject, action: ComicListActions) => {
  switch (action.type) {
    case "SET_COMICS":
      return {
        ...context,
        state: {
          ...context.state,
          comics: action.comics
        }
      };

    case 'ADD_FAVOURITE':
      return {
        ...context,
        state: {
          ...context.state,
          favourites: [ ...context.state.favourites, action.comic ]
        }
      };

    case 'REMOVE_FAVOURITE':
      return {
        ...context,
        state: {
          ...context.state,
          favourites: [ ...context.state.favourites.filter((item: ComicListItem) => item !== action.comic) ]
        }
      };

    default:
      return context;
  }
};  

// Dispatch wrappers...
// --------------------------------------------------------------------------------------
// This first one, setComics, will be 'private' (see ComicListContextProvider), 
// and only used to retrieve an initial list of data from the API.
export const setComics = async (dispatch: React.Dispatch<ComicListActions>, comicApiEndpoint: string) => {
  const response = await fetch(comicApiEndpoint);
  const responseJson: any = await response.json();

  if(responseJson?.data?.results.length) {
    dispatch({
      type: "SET_COMICS", 
      comics: responseJson?.data?.results.map((item: any) => {
        return { 
          id: item.id, 
          title: item.title, 
          images: item.images.map((image: any) => {
            return { path: image.path, extension: image.extension }
          }) 
        };
      })
    });
  }
};

// The next two dispatch wrappers are used to update favourites, and will always be available from
// ComicListContext.actions.addFavourite, and ComicListContext.actions.removeFavourite.
// So it should be easy to see how a more complicated store stucture can be built out of these elementary constructs.
export const addFavourite = (dispatch: React.Dispatch<ComicListActions>) => {
  return (comic: ComicListItem) => {
    dispatch({
      type: "ADD_FAVOURITE", 
      comic
    })
  }
};

// The only irritating issue, is in needing to pass the dispatch property from the
// useReducer call, down to these higher order functions.
export const removeFavourite = (dispatch: React.Dispatch<ComicListActions>) => {
  return (comic: ComicListItem) => {
    dispatch({
      type: "REMOVE_FAVOURITE", 
      comic
    })
  }
};