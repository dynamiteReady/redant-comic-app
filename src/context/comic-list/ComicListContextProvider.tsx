import { useReducer } from 'react';
import { ComicListContext, InitialComicListContext } from './ComicListContext';
import { addFavourite, comicListReducer, setComics, removeFavourite } from './ComicListContextActions';
import { ComicListContextProviderProps } from './ComicListContextStateTypes';

const comicApiEndpoint = "https://gateway.marvel.com/v1/public/comics?apikey=3cb62d086d5debdeea139095cbb07fe4&ts=redant&hash=140e85a50884cef76d614f6dacada288";

// This component is essentially bolerplate, but it's used to register consumable dispatch wrappers,
// and load the initial list of comics from the API.
export const ComicListContextProvider = ({ children }: ComicListContextProviderProps) => {
  const [context, dispatch] = useReducer(comicListReducer, InitialComicListContext);
  
  const setComicList = dispatch;

  const comicListContextObject = { 
    ...context,
    actions: {
      // Register our public context action dispatch wrapper functions from ComicListContextActions here.
      addFavourite: addFavourite(dispatch), 
      removeFavourite: removeFavourite(dispatch) 
    }
  };
  
  // Only retrieve the API data, if the list is not already in the store/context.
  // If we were adding pagination, and possibly, caching, then the logic would have to change.
  // But in this simple case, we just want to prevent unneccessary API calls.
  if(!context.state.comics.length) {
    setComics(dispatch, comicApiEndpoint);
  }

  return <ComicListContext.Provider value={{ ...comicListContextObject, ...setComicList }}>{ children }</ComicListContext.Provider>;
}