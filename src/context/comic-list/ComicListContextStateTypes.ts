import React from "react";

export type ComicListItem = {
  id: string;
  title: string;
  images?: ComicListImageItem[];
}

export type ComicListImageItem = {
  path: string;
  extension: string;
}

export type ComicListContextState = {
  comics: ComicListItem[];
  favourites: ComicListItem[];
}

export type ComicListContextActions = {
  addFavourite: (comic: ComicListItem) => void;
  removeFavourite: (comic: ComicListItem) => void;
}

export type ComicListContextObject = {
  state: ComicListContextState;
  actions: ComicListContextActions;
};

export type ComicListContextProviderProps = {
  children: React.ReactNode
}