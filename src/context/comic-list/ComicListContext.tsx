import { createContext } from "react";
import { ComicListContextObject, ComicListItem } from "./ComicListContextStateTypes";

// You could start by looking at the types, but I'd recommend starting here first, to see
// exactly what the ComicListContext will do. 
export const InitialComicListContext: ComicListContextObject = {
    state: {
      comics: [],
      favourites: [],
    },
  
    actions: {
      // Will override these consumable functions in ComicListContextProvider, with
      // the dispatch wrappers found in ComicListContextActions
      addFavourite: (comic: ComicListItem) => {},
      removeFavourite: (comic: ComicListItem) => {} 
    }
  };

export const ComicListContext = createContext<ComicListContextObject>(InitialComicListContext);