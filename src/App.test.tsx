import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { App } from './App';

jest.setTimeout(15000);

describe("<App/>", () => {
  beforeEach(() => {
    render(<App/>);
  });

  describe("<Header/>", () => {
    it('Should display correct title', () => {
      const title = screen.getByText("Red Ant Comics");
      expect(title).toBeInTheDocument();
    });
  
    it('Should display a count of (0) favourites', () => {
      const favouritesIndicator = screen.getByText("Favourites: (0)");
      expect(favouritesIndicator).toBeInTheDocument();
    });

    it('Should display a count of (1) favourites when an item is favourited', async () => {
      const { container } = render(<App/>);
      
      await waitFor(() => {
        const eventTargetElement = container.querySelector(".comics-list .item")?.firstChild;
        
        if(eventTargetElement) fireEvent.click(eventTargetElement);
        
        const favouritesIndicator = screen.getByText("Favourites: (1)");

        expect(favouritesIndicator).toBeInTheDocument();
      }, { timeout: 10000 });
    });     
  });

  describe("<ComicList/>", () => {
    it('Should display loading, by default', () => {
      const title = screen.getByText("Loading Comicbook Data...");
      expect(title).toBeInTheDocument();
    });

    it('Should display loading, by default', async () => {
      const { container } = render(<App/>);

      await waitFor(() => {
        const element = container.querySelector(".comics-list");
        expect(element).not.toBe(null);
      }, { timeout: 10000 });
    });

    it('Should indicate when an item is favourited', async () => {
      // Possible bug in Jest?...
      // This test works, but if you change the selectors to non existent ones,
      // the suite will NEVER timeout! O.O
      const { container } = render(<App/>);

      await waitFor(() => {
        const element = container.querySelector(".comics-list .item.selected");
        const eventTargetElement = container.querySelector(".comics-list .item")?.firstChild;

        if(eventTargetElement) fireEvent.click(eventTargetElement);

        expect(element).not.toBe(null);
      }, { timeout: 10000 });
    });    
  });
});

